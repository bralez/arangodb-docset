# Build script for Dash docset

## Useful links

[ArangoDB online documentation](https://www.arangodb.com/documentation/)

[Docset generation guide](https://kapeli.com/docsets)

## Requirements

* [Dashing](https://github.com/technosophos/dashing)

### Installation on OSX

```
brew install dashing
```

## Usage

From command shell:
```
$ ./generate-docset.sh
```