#!/usr/bin/env bash

ARANGODB_VERSION=3.3.5

rm -rf ArangoDB-HTML

curl https://download.arangodb.com/arangodb33/doc/ArangoDB-$ARANGODB_VERSION.tar.gz | tar xz

mv ArangoDB-$ARANGODB_VERSION ArangoDB-HTML

find ./ArangoDB-HTML/AQL -name "index.html" -exec sed -i '' 's/<title>\(.*\) · ArangoDB v3.3.5 AQL Documentation<\/title>/<title class="dash-guide">AQL · \1<\/title>/' {} \;
find ./ArangoDB-HTML/Cookbook -name "index.html" -exec sed -i '' 's/<title>\(.*\) · ArangoDB Cookbook<\/title>/<title class="dash-guide">Cookbook · \1<\/title>/' {} \;
find ./ArangoDB-HTML/HTTP -name "index.html" -exec sed -i '' 's/<title>\(.*\) · ArangoDB v3.3.5 HTTP API Documentation<\/title>/<title class="dash-guide">HTTP · \1<\/title>/' {} \;
find ./ArangoDB-HTML/Manual -name "index.html" -exec sed -i '' 's/<title>\(.*\) · ArangoDB v3.3.5 Documentation<\/title>/<title class="dash-guide">Manual · \1<\/title>/' {} \;

dashing build --source ./ArangoDB-HTML arangodb
